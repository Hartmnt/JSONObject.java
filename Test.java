package json;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import publicdomain.JSON;

public class Test {

	private static int no = 0;
	private static int pack = 0;
	private static int failedcount = 0;
	private static int passedcount = 0;
	private static boolean failed = false;

	public static void main(String[] args) {
		JSON.ESCAPE_FORWARD_SLASHES = false;
		nextPack();

		//---------- TEST FOR NULL ---------------
		test(JSON.JSONVALUE_TYPE.NULL, null);
		test("", JSON.JSONVALUE_TYPE.NULL, null);
		test(" ", JSON.JSONVALUE_TYPE.NULL, null);
		test(" \u0020", JSON.JSONVALUE_TYPE.NULL, null);
		test(" \u0020\n", JSON.JSONVALUE_TYPE.NULL, null);
		test("\t \u0020\n", JSON.JSONVALUE_TYPE.NULL, null);
		test("\t \u0020\r\n", JSON.JSONVALUE_TYPE.NULL, null);
		test("null", JSON.JSONVALUE_TYPE.NULL, null);
		test("\t\u0020 \r\nnull", JSON.JSONVALUE_TYPE.NULL, null);
		test("\t\u0020 \r\nnull\t\u0020 \r\n", JSON.JSONVALUE_TYPE.NULL, null);
		test("n", IllegalStateException.class);
		test("nu", IllegalStateException.class);
		test("nul", IllegalStateException.class);
		test("n ull", IllegalStateException.class);
		test("nul l", IllegalStateException.class);
		test("nul\u0020l", IllegalStateException.class);
		test("nnull", IllegalStateException.class);
		test("nulll", IllegalStateException.class);
		test("NULL", IllegalStateException.class);
		test("Null", IllegalStateException.class);
		test("nulL", IllegalStateException.class);
		test("", "null");
		nextPack();

		//---------- TEST FOR TRUE ---------------
		test(JSON.JSONVALUE_TYPE.TRUE, true);
		test("true", JSON.JSONVALUE_TYPE.TRUE, true);
		test(" true", JSON.JSONVALUE_TYPE.TRUE, true);
		test("true ", JSON.JSONVALUE_TYPE.TRUE, true);
		test("\t \u0020\r\ntrue\t \u0020\r\n", JSON.JSONVALUE_TYPE.TRUE, true);
		test("t", IllegalStateException.class);
		test("tr", IllegalStateException.class);
		test("tru", IllegalStateException.class);
		test("True", IllegalStateException.class);
		test("truE", IllegalStateException.class);
		test("TRUE", IllegalStateException.class);
		test("tr ue", IllegalStateException.class);
		test("tru\u0020e", IllegalStateException.class);
		test("true", "true");
		nextPack();

		//---------- TEST FOR FALSE ---------------
		test(JSON.JSONVALUE_TYPE.FALSE, false);
		test("false", JSON.JSONVALUE_TYPE.FALSE, false);
		test(" false", JSON.JSONVALUE_TYPE.FALSE, false);
		test("false ", JSON.JSONVALUE_TYPE.FALSE, false);
		test("\t \u0020\r\nfalse\t \u0020\r\n", JSON.JSONVALUE_TYPE.FALSE, false);
		test("f", IllegalStateException.class);
		test("fa", IllegalStateException.class);
		test("fal", IllegalStateException.class);
		test("fals", IllegalStateException.class);
		test("False", IllegalStateException.class);
		test("falsE", IllegalStateException.class);
		test("FALSE", IllegalStateException.class);
		test("fa lse", IllegalStateException.class);
		test("fals\u0020e", IllegalStateException.class);
		test("false", "false");
		nextPack();

		//---------- TEST FOR STRING ---------------
		test(JSON.JSONVALUE_TYPE.JSON_STRING, "");
		test("\"\"", JSON.JSONVALUE_TYPE.JSON_STRING, "");
		test("\"This is a string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This is a string");
		test(" \"This is a string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This is a string");
		test(" \"This is a string\" ", JSON.JSONVALUE_TYPE.JSON_STRING, "This is a string");
		test("\t \u0020\r\n\"This is a string\"\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_STRING, "This is a string");
		test("\"This\\tis a string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This\tis a string");
		test("\"This is a\\nstring\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This is a\nstring");
		test("\"This\\u0020is\\u0020a\\u0020string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This is a string");
		test("\"This\\\"is a string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This\"is a string");
		test("\"This\\\"is a\\\"string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This\"is a\"string");
		test("\"This is \\u0041 string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This is A string");
		test("\"This is\\\\a string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This is\\a string");
		test("\"This is\\/a string\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This is/a string");
		test("\"This\\bis\\fa\\r\\nstring\\t\"", JSON.JSONVALUE_TYPE.JSON_STRING, "This\bis\fa\r\nstring\t");
		test("\"\\u0000\"", JSON.JSONVALUE_TYPE.JSON_STRING, "\u0000");
		test("\"\\t\"", JSON.JSONVALUE_TYPE.JSON_STRING, "\t");
		test("\"\\n\"", JSON.JSONVALUE_TYPE.JSON_STRING, "\n");
		test("\"", IllegalStateException.class);
		test("\"This is a string", IllegalStateException.class);
		test("\"\\\"", IllegalStateException.class);
		test("\"\u0000\"", IllegalStateException.class);
		test("\"\\u005\"", IllegalStateException.class);
		test("\"\n\"", IllegalStateException.class);
		test("\"This is\ta string\"", IllegalStateException.class);
		test("\"This is\na string\"", IllegalStateException.class);
		test("\"This is\ra string\"", IllegalStateException.class);
		test("\"This is\u0010a string\"", IllegalStateException.class);
		test("\"\"", "\"\"");
		test("\"This is a string\"", "\"This is a string\"");
		test("\" \\\\ \"", "\" \\\\ \"");
		test("\" \\\" \\\\ \\b \\f \\n \\r \\t \\/ \\u0015 \"", "\" \\\" \\\\ \\b \\f \\n \\r \\t / \\u0015 \"");
		JSON.ESCAPE_FORWARD_SLASHES = true;
		test("\" \\\" \\\\ \\b \\f \\n \\r \\t \\/ \\u0015 \"", "\" \\\" \\\\ \\b \\f \\n \\r \\t \\/ \\u0015 \"");
		JSON.ESCAPE_FORWARD_SLASHES = false;
		nextPack();

		//---------- TEST FOR INTEGER ---------------
		test(JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		test("0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		test(" 0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		test("0 ", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		test("\t \u0020\r\n0\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		test("123", JSON.JSONVALUE_TYPE.JSON_NUMBER, 123);
		test(" 123", JSON.JSONVALUE_TYPE.JSON_NUMBER, 123);
		test("123 ", JSON.JSONVALUE_TYPE.JSON_NUMBER, 123);
		test("\t \u0020\r\n123\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_NUMBER, 123);
		test("-123", JSON.JSONVALUE_TYPE.JSON_NUMBER, -123);
		test(" -123", JSON.JSONVALUE_TYPE.JSON_NUMBER, -123);
		test("-123 ", JSON.JSONVALUE_TYPE.JSON_NUMBER, -123);
		test("\t \u0020\r\n-123\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_NUMBER, -123);
		test(Integer.toString(Integer.MAX_VALUE), JSON.JSONVALUE_TYPE.JSON_NUMBER, Integer.MAX_VALUE);
		test(Integer.toString(Integer.MIN_VALUE), JSON.JSONVALUE_TYPE.JSON_NUMBER, Integer.MIN_VALUE);
		test(Long.toString((long) Integer.MAX_VALUE + 1), JSON.JSONVALUE_TYPE.JSON_NUMBER, (long) Integer.MAX_VALUE + 1);
		test(Long.toString((long) Integer.MIN_VALUE - 1), JSON.JSONVALUE_TYPE.JSON_NUMBER, (long) Integer.MIN_VALUE - 1);
		test(Long.toString(Long.MAX_VALUE), JSON.JSONVALUE_TYPE.JSON_NUMBER, Long.MAX_VALUE);
		test(Long.toString(Long.MIN_VALUE), JSON.JSONVALUE_TYPE.JSON_NUMBER, Long.MIN_VALUE);
		test("-", IllegalStateException.class);
		test("10 05", IllegalStateException.class);
		test("- 10", IllegalStateException.class);
		test("10-10", IllegalStateException.class);
		nextPack();

		//---------- TEST FOR DOUBLE ---------------
		test("-0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		test("0.0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0.0);
		test("0.01", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0.01);
		test("-0.0", JSON.JSONVALUE_TYPE.JSON_NUMBER, -0.0);
		test("-0.01", JSON.JSONVALUE_TYPE.JSON_NUMBER, -0.01);
		test("0.123456", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0.123456);
		test("-0.123456", JSON.JSONVALUE_TYPE.JSON_NUMBER, -0.123456);
		test("1.0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 1.0);
		test("1.01", JSON.JSONVALUE_TYPE.JSON_NUMBER, 1.01);
		test("-1.0", JSON.JSONVALUE_TYPE.JSON_NUMBER, -1.0);
		test("-1.01", JSON.JSONVALUE_TYPE.JSON_NUMBER, -1.01);
		test("1.123456", JSON.JSONVALUE_TYPE.JSON_NUMBER, 1.123456);
		test("-1.123456", JSON.JSONVALUE_TYPE.JSON_NUMBER, -1.123456);
		test("123.0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 123.0);
		test("123.01", JSON.JSONVALUE_TYPE.JSON_NUMBER, 123.01);
		test("-123.0", JSON.JSONVALUE_TYPE.JSON_NUMBER, -123.0);
		test("-123.01", JSON.JSONVALUE_TYPE.JSON_NUMBER, -123.01);
		test("123.123456", JSON.JSONVALUE_TYPE.JSON_NUMBER, 123.123456);
		test("-123.123456", JSON.JSONVALUE_TYPE.JSON_NUMBER, -123.123456);
		test("0e0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0e+0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0e-0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0e10", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0e+10", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0e-10", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0E0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0E+0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0E-0", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0E10", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0E+10", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("0E-10", JSON.JSONVALUE_TYPE.JSON_NUMBER, 0e0);
		test("12e1", JSON.JSONVALUE_TYPE.JSON_NUMBER, 12e1);
		test("12e+1", JSON.JSONVALUE_TYPE.JSON_NUMBER, 12e+1);
		test("12e-1", JSON.JSONVALUE_TYPE.JSON_NUMBER, 12e-1);
		test("-12e1", JSON.JSONVALUE_TYPE.JSON_NUMBER, -12e1);
		test("-12e+1", JSON.JSONVALUE_TYPE.JSON_NUMBER, -12e+1);
		test("-12e-1", JSON.JSONVALUE_TYPE.JSON_NUMBER, -12e-1);
		test("-12.12e1", JSON.JSONVALUE_TYPE.JSON_NUMBER, -12.12e1);
		test("-12.12e-1", JSON.JSONVALUE_TYPE.JSON_NUMBER, -12.12e-1);
		test("-12.12e+1", JSON.JSONVALUE_TYPE.JSON_NUMBER, -12.12e+1);
		test("12.12e1", JSON.JSONVALUE_TYPE.JSON_NUMBER, 12.12e1);
		test("12.12e-1", JSON.JSONVALUE_TYPE.JSON_NUMBER, 12.12e-1);
		test("12.12e+1", JSON.JSONVALUE_TYPE.JSON_NUMBER, 12.12e+1);
		test("0e", IllegalStateException.class);
		test("0 e", IllegalStateException.class);
		test("0 .123", IllegalStateException.class);
		test("1 2.123", IllegalStateException.class);
		test("12.12 3", IllegalStateException.class);
		test("0e+", IllegalStateException.class);
		test("0e-", IllegalStateException.class);
		test("12e-", IllegalStateException.class);
		test(".", IllegalStateException.class);
		test(".0123", IllegalStateException.class);
		test(".0123e10", IllegalStateException.class);
		test(".0123E10", IllegalStateException.class);
		test(".0123e+10", IllegalStateException.class);
		test(".0123e-10", IllegalStateException.class);
		test("0", "0");
		test("123", "123");
		test("123.123", "123.123");
		test("-0", "0");
		test("-123", "-123");
		test("-123.123", "-123.123");
		nextPack();

		//---------- TEST FOR ARRAY ---------------
		ArrayList<Object> array_empty = new ArrayList<>(Arrays.asList());
		test(JSON.JSONVALUE_TYPE.JSON_ARRAY, array_empty);
		test("[]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_empty);
		test(" []", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_empty);
		test("[] ", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_empty);
		test("[ ]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_empty);
		test("\t \u0020\r\n[\t \u0020\r\n]\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_empty);
		test("[", IllegalStateException.class);
		test("[[", IllegalStateException.class);
		test("]", IllegalStateException.class);
		test("]]", IllegalStateException.class);
		test("[,]", IllegalStateException.class);
		test("[ , ]", IllegalStateException.class);
		test("[],", IllegalStateException.class);
		test(",[]", IllegalStateException.class);
		test("[]", "[]");

		ArrayList<Object> array_single = new ArrayList<>(Arrays.asList((Object) null));
		test("[null]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_single);
		test(" [null]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_single);
		test("[null] ", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_single);
		test("[ null]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_single);
		test("[ null ]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_single);
		test("\t \u0020\r\n[\t \u0020\r\nnull\t \u0020\r\n]\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_single);
		test("[null", IllegalStateException.class);
		test("[null,", IllegalStateException.class);
		test("[null,]", IllegalStateException.class);
		test("[,null]", IllegalStateException.class);
		test("[ , null , ]", IllegalStateException.class);
		test("[null]", "[null]");

		ArrayList<Object> array_two = new ArrayList<>(Arrays.asList(true, false));
		test("[true,false]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_two);
		test(" [true,false]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_two);
		test("[true,false] ", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_two);
		test("[ true,false]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_two);
		test("[ true, false ]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_two);
		test("\t \u0020\r\n[\t \u0020\r\ntrue\t \u0020\r\n,\t \u0020\r\nfalse\t \u0020\r\n]\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_two);
		test("[ true, false, ]", IllegalStateException.class);
		test("[ , true, false ]", IllegalStateException.class);
		test("[ , true, false ]", IllegalStateException.class);
		test("[ true, false", IllegalStateException.class);
		test("[ true, false,", IllegalStateException.class);
		test("[ true, false ,]", IllegalStateException.class);
		test("[true,false]", "[true,false]");

		ArrayList<Object> array_strings = new ArrayList<>(Arrays.asList("This is a string", "This is another string", "This is a third string"));
		test("[\"This is a string\",\"This is another string\",\"This is a third string\"]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_strings);
		test(" [\"This is a string\",\"This is another string\",\"This is a third string\"]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_strings);
		test("[\"This is a string\",\"This is another string\",\"This is a third string\"] ", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_strings);
		test("[ \"This is a string\",\"This is another string\",\"This is a third string\"]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_strings);
		test("[\"This is a string\", \"This is another string\" ,\"This is a third string\"]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_strings);
		test("[\"This is a string\",\"This is another string\",\"This is a third string\" ]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_strings);
		test("\t \u0020\r\n[\t \u0020\r\n\"This is a string\"\t \u0020\r\n,\t \u0020\r\n\"This is another string\"\t \u0020\r\n,\t \u0020\r\n\"This is a third string\"\t \u0020\r\n]\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_strings);
		test("[\"This is a string\" \"This is another string\",\"This is a third string\"]", IllegalStateException.class);
		test("[\"This is a string\",\"This is another string\",\"This is a third string\" ", IllegalStateException.class);
		test("[\"This is a string\",\"This is another string\",\"This is a third string\", ", IllegalStateException.class);
		test("[\"This is a string\",\"This is another string\",\"This is a third string\",]", IllegalStateException.class);
		test("[\"This is a string,\"This is another string\",\"This is a third string\"]", IllegalStateException.class);
		test("[\"This is a string\",\"This is another string\",\"This is a third string\"]", "[\"This is a string\",\"This is another string\",\"This is a third string\"]");

		ArrayList<Object> array_seperator = new ArrayList<>(Arrays.asList("This, is] ,a ,string,"));
		test("[\"This, is] ,a ,string,\"]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_seperator);
		test("[\"This, is\"] ,a ,string,\"]", IllegalStateException.class);
		test("[\"This, is] ,a ,string,\"]", "[\"This, is] ,a ,string,\"]");

		ArrayList<Object> array_arrayarray = new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList())));
		test("[[]]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_arrayarray);
		test("[ []]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_arrayarray);
		test("[[] ]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_arrayarray);
		test("[\t \u0020\r\n[\t \u0020\r\n]\t \u0020\r\n]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_arrayarray);
		test("[[]", IllegalStateException.class);
		test("[]]", IllegalStateException.class);
		test("[,[]]", IllegalStateException.class);
		test("[[],]", IllegalStateException.class);
		test("[ , [] ]", IllegalStateException.class);
		test("[ [] ,]", IllegalStateException.class);
		test("[, [] ,]", IllegalStateException.class);
		test("[[]]", "[[]]");

		ArrayList<Object> array_arrays = new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(true, false)), new ArrayList<>(Arrays.asList(false, true))));
		test("[[true, false], [false, true]]", JSON.JSONVALUE_TYPE.JSON_ARRAY, array_arrays);
		test("[[true, false], false, true]]", IllegalStateException.class);
		test("[[true, false], [false, true]", IllegalStateException.class);
		test("[[true, false], [false, true], ", IllegalStateException.class);
		test("[true, false], [false, true]", IllegalStateException.class);
		test("[true, false], [false, true], ", IllegalStateException.class);
		test("[[true, false], [false, true]]]", IllegalStateException.class);
		test("[[[true, false], [false, true]]", IllegalStateException.class);
		test("[[true, false],[false, true]]", "[[true,false],[false,true]]");
		nextPack();

		//---------- TEST FOR OBJECT ---------------
		HashMap<String, Object> object_empty = new HashMap<>();
		test(JSON.JSONVALUE_TYPE.JSON_OBJECT, object_empty);
		test("{}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_empty);
		test(" {}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_empty);
		test("{} ", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_empty);
		test("{ }", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_empty);
		test("\t \u0020\r\n{\t \u0020\r\n}\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_empty);
		test("{", IllegalStateException.class);
		test("{{", IllegalStateException.class);
		test("}", IllegalStateException.class);
		test("}}", IllegalStateException.class);
		test("{,}", IllegalStateException.class);
		test("{ , }", IllegalStateException.class);
		test("{},", IllegalStateException.class);
		test(",{}", IllegalStateException.class);
		test("{:}", IllegalStateException.class);
		test("{:,}", IllegalStateException.class);
		test("{,:,}", IllegalStateException.class);
		test("{}", "{}");

		HashMap<String, Object> object_single = new HashMap<>();
		object_single.put("key", null);
		test("{\"key\":null}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_single);
		test("{ \"key\" : null }", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_single);
		test("\t \u0020\r\n{\t \u0020\r\n\"key\"\t \u0020\r\n:\t \u0020\r\nnull\t \u0020\r\n}\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_single);
		test("{\"key\":null,}", IllegalStateException.class);
		test("{,\"key\":null}", IllegalStateException.class);
		test("{ , \"key\":null }", IllegalStateException.class);
		test("{ \"key\":null } , ", IllegalStateException.class);
		test("{ \"key\":null , }", IllegalStateException.class);
		test("{'key':null}", IllegalStateException.class);
		test("{key:null}", IllegalStateException.class);
		test("{123:null}", IllegalStateException.class);
		test("{\"key\" null}", IllegalStateException.class);
		test("{\"key\", null}", IllegalStateException.class);
		test("{\"key\":null}", "{\"key\":null}");

		HashMap<String, Object> object_two = new HashMap<>();
		object_two.put("key1", true);
		object_two.put("key2", false);
		test("{\"key1\":true,\"key2\":false}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_two);
		test("{\"key2\":false,\"key1\":true}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_two);
		test("{ \"key1\":true, \"key2\":false }", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_two);
		test("\t \u0020\r\n{\t \u0020\r\n\"key1\"\t \u0020\r\n:\t \u0020\r\ntrue\t \u0020\r\n,\t \u0020\r\n\"key2\"\t \u0020\r\n:\t \u0020\r\nfalse\t \u0020\r\n}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_two);
		test("{\"key1\" true,\"key2\":false}", IllegalStateException.class);
		test("{\"key1\":true \"key2\":false}", IllegalStateException.class);
		test("{\"key1\":true,\"key2\" false}", IllegalStateException.class);
		test("\"key1\":true,\"key2\":false}", IllegalStateException.class);
		test("{\"key1\":true,\"key2\":false", IllegalStateException.class);
		test("{\"key1\":\"key2\":false}", IllegalStateException.class);
		test("{\"key1\":true,\"key2\":false,}", IllegalStateException.class);
		test("{\"key1\":true,\"key2\":false}", "{\"key1\":true,\"key2\":false}");

		HashMap<String, Object> object_strings = new HashMap<>();
		object_strings.put("key1", "This is a string");
		object_strings.put("key2", "This is another string");
		object_strings.put("key3", "This is a thrid string");
		test("{\"key1\":\"This is a string\",\"key2\":\"This is another string\",\"key3\":\"This is a thrid string\"}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_strings);
		test("{\"key3\":\"This is a thrid string\",\"key1\":\"This is a string\",\"key2\":\"This is another string\"}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_strings);
		test("\t \u0020\r\n{\t \u0020\r\n\"key3\":\t \u0020\r\n\"This is a thrid string\"\t \u0020\r\n,\t \u0020\r\n\"key1\":\"This is a string\",\"key2\":\"This is another string\"\t \u0020\r\n}\t \u0020\r\n", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_strings);
		test("{\"key1\":\"This is a string\" \"key2\":\"This is another string\",\"key3\":\"This is a thrid string\"}", IllegalStateException.class);
		test("{\"key1\": This is a string\",\"key2\":\"This is another string\",\"key3\":\"This is a thrid string\"}", IllegalStateException.class);
		test("{\"key1 This is a string\",\"key2\":\"This is another string\",\"key3\":\"This is a thrid string\"}", IllegalStateException.class);
		test("{\"key1\":\"This is a string\",\"key2\":\"This is another string\"},{\"key3\":\"This is a thrid string\"}", IllegalStateException.class);
		test("{\"key1\":\"This is a string\",\"key2\":\"This is another string\",\"key3\":\"This is a thrid string\"}", "{\"key1\":\"This is a string\",\"key2\":\"This is another string\",\"key3\":\"This is a thrid string\"}");

		HashMap<String, Object> object_seperators = new HashMap<>();
		object_seperators.put("key1", "This\" ,is :a \"]string");
		test("{\"key1\":\"This\\\" ,is :a \\\"]string\"}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_seperators);
		test("{\"key1\":\"This\\\" ,is\":a \\\"]string\"}", IllegalStateException.class);
		test("{\"key1\":\"This\\\" ,is :a \\\"]string\"}", "{\"key1\":\"This\\\" ,is :a \\\"]string\"}");

		HashMap<String, Object> object_objectobject = new HashMap<>();
		object_objectobject.put("key1", new HashMap());
		test("{\"key1\":{}}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_objectobject);
		test("{ \"key1\" : {} }", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_objectobject);
		test("{ \"key1\" : { } }", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_objectobject);
		test("\t \u0020\r\n{\t \u0020\r\n\"key1\"\t \u0020\r\n:\t \u0020\r\n{\t \u0020\r\n}\t \u0020\r\n}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_objectobject);
		test("{\"key1\":{}", IllegalStateException.class);
		test("{\"key1\":}}", IllegalStateException.class);
		test("{,\"key1\":{}}", IllegalStateException.class);
		test("{\"key1\":{},}", IllegalStateException.class);
		test("{ , \"key1\":{} }", IllegalStateException.class);
		test("{ \"key1\":{} ,}", IllegalStateException.class);
		test("{, \"key1\":{} ,}", IllegalStateException.class);
		test("{\"key1\":{}}", "{\"key1\":{}}");

		HashMap<String, Object> object_objects = new HashMap<>();
		HashMap<String, Object> object_objects1 = new HashMap<>();
		object_objects1.put("key1", true);
		HashMap<String, Object> object_objects2 = new HashMap<>();
		object_objects2.put("key2", false);
		object_objects.put("key1", object_objects1);
		object_objects.put("key2", object_objects2);
		test("{\"key1\":{\"key1\":true},\"key2\":{\"key2\":false}}", JSON.JSONVALUE_TYPE.JSON_OBJECT, object_objects);
		test("\"key1\":{\"key1\":true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{key1\":{\"key1\":true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1:{\"key1\":true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\"{\"key1\":true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":\"key1\":true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{key1\":true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1:true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\"true},\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true,\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true}\"key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},key2\":{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2:{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2\"{\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2\":\"key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2\":{key2\":false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2\":{\"key2:false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2\":{\"key2\"false}}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2\":{\"key2\":false}", IllegalStateException.class);
		test("{\"key1\":{\"key1\":true},\"key2\":{\"key2\":false}}", "{\"key1\":{\"key1\":true},\"key2\":{\"key2\":false}}");
		nextPack();

		//---------- TEST COPY ---------------
		test("null");
		test("true");
		test("false");
		test("\"This is a string\"");
		test("123");
		test("123.123");
		test("-123");
		test("[null,true,false,\"This is a string\",[null,true,false]]");
		test("{\"key1\":{\"key1\":true},\"key2\":{\"key2\":false}}");
		nextPack();

		//---------- TEST PUBLIC API ---------------
		testObject(JSON.JSONVALUE_TYPE.NULL, null);
		testObject(JSON.JSONVALUE_TYPE.TRUE, true);
		testObject(JSON.JSONVALUE_TYPE.FALSE, false);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, 123);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, -123);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, -0);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, 0.5);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, 123.123);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, -0.5);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, -123.123);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, 0.5f);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, 123.123f);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, -0.5f);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, -123.123f);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, Long.MAX_VALUE);
		testObject(JSON.JSONVALUE_TYPE.JSON_NUMBER, Long.MIN_VALUE);
		testObject(JSON.JSONVALUE_TYPE.JSON_STRING, "This is a string");
		testObject(JSON.JSONVALUE_TYPE.JSON_ARRAY, new JSON().json_add(null).json_add(null));
		testObject(JSON.JSONVALUE_TYPE.JSON_OBJECT, new JSON().json_add("key1", null).json_add("key2", null));
		nextPack();

		testArray(JSON.JSONVALUE_TYPE.NULL, null);
		testArray(JSON.JSONVALUE_TYPE.TRUE, true);
		testArray(JSON.JSONVALUE_TYPE.FALSE, false);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, 0);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, 123);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, -123);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, -0);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, 0.5);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, 123.123);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, -0.5);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, -123.123);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, 0.5f);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, 123.123f);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, -0.5f);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, -123.123f);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, Long.MAX_VALUE);
		testArray(JSON.JSONVALUE_TYPE.JSON_NUMBER, Long.MIN_VALUE);
		testArray(JSON.JSONVALUE_TYPE.JSON_STRING, "This is a string");
		testArray(JSON.JSONVALUE_TYPE.JSON_ARRAY, new JSON().json_add(null).json_add(null));
		testArray(JSON.JSONVALUE_TYPE.JSON_OBJECT, new JSON().json_add("key1", null).json_add("key2", null));
		nextPack();

		//---------- TEST ITERATOR ---------------
		testIterator(new JSON(), Arrays.asList());
		testIterator(new JSON("null"), Arrays.asList());
		testIterator(new JSON("[null]"), Arrays.asList((Object) null));
		testIterator(new JSON().json_add(null), Arrays.asList((Object) null));
		testIterator(new JSON("true"), Arrays.asList(true));
		testIterator(new JSON("[true]"), Arrays.asList(true));
		testIterator(new JSON().json_add(true), Arrays.asList(true));
		testIterator(new JSON("false"), Arrays.asList(false));
		testIterator(new JSON("[false]"), Arrays.asList(false));
		testIterator(new JSON().json_add(false), Arrays.asList(false));
		testIterator(new JSON("0"), Arrays.asList(0));
		testIterator(new JSON("[0]"), Arrays.asList(0));
		testIterator(new JSON().json_add(0), Arrays.asList(0));
		testIterator(new JSON("123"), Arrays.asList(123));
		testIterator(new JSON().json_add(123), Arrays.asList(123));
		testIterator(new JSON("123.123"), Arrays.asList(123.123));
		testIterator(new JSON().json_add(123.123), Arrays.asList(123.123));
		testIterator(new JSON("-0"), Arrays.asList(0));
		testIterator(new JSON().json_add(-0), Arrays.asList(0));
		testIterator(new JSON("-123"), Arrays.asList(-123));
		testIterator(new JSON().json_add(-123), Arrays.asList(-123));
		testIterator(new JSON("-123.123"), Arrays.asList(-123.123));
		testIterator(new JSON().json_add(-123.123), Arrays.asList(-123.123));
		testIterator(new JSON("\"This is a string\""), Arrays.asList("This is a string"));
		testIterator(new JSON().json_add("This is a string"), Arrays.asList("This is a string"));
		testIterator(new JSON("[\"This is a string\"]"), Arrays.asList("This is a string"));
		testIterator(new JSON("[\"This is a string\", null, true, false]"), Arrays.asList("This is a string", null, true, false));
		testIterator(new JSON("[[0]]"), Arrays.asList(new JSON().json_add(0)));
		testIterator(new JSON("[[0],[1]]"), Arrays.asList(new JSON().json_add(0), new JSON().json_add(1)));
		testIterator(new JSON("[{\"key\": 0}]"), Arrays.asList(new JSON().json_add("key", 0)));
		testIterator(new JSON("[{\"key\": 0}, {\"key2\": 1}]"), Arrays.asList(new JSON().json_add("key", 0), new JSON().json_add("key2", 1)));
		testIterator(new JSON("{\"key\": 0}"), Arrays.asList(new AbstractMap.SimpleEntry<>("key", 0)));
		testIterator(new JSON("{\"key1\": 0, \"key2\": [0], \"key3\": 2}"), Arrays.asList(new AbstractMap.SimpleEntry<>("key1", 0), new AbstractMap.SimpleEntry<>("key2", new JSON().json_add(0)), new AbstractMap.SimpleEntry<>("key3", 2)));
		nextPack();

		//---------- TEST ITERATOR ---------------
		testIteratorCast(new JSON("null"), new LinkedList<>());
		testIteratorCast(new JSON("true"), Arrays.asList(true));
		testIteratorCast(new JSON("false"), Arrays.asList(false));
		testIteratorCast(new JSON("[true, false, true]"), Arrays.asList(true, false, true));
		testIteratorCast(new JSON("0"), Arrays.asList(0));
		testIteratorCast(new JSON("[0,1,2]"), Arrays.asList(0, 1, 2));
		testIteratorCast(new JSON("0.5"), Arrays.asList(0.5));
		testIteratorCast(new JSON("[0.1,1.2,2.3]"), Arrays.asList(0.1, 1.2, 2.3));
		testIteratorCast(new JSON("\"ASD\""), Arrays.asList("ASD"));
		testIteratorCast(new JSON("[\"ASD\",\"SAD\",\"DSA\"]"), Arrays.asList("ASD", "SAD", "DSA"));
		testIteratorCast(new JSON("0"), Arrays.asList(0));
		testIteratorCast(new JSON("[[1]]"), Arrays.asList(new JSON("[1]")));
		testIteratorCast(new JSON("[[1],[2],[3]]"), Arrays.asList(new JSON("[1]"), new JSON("[2]"), new JSON("[3]")));
		testIteratorCast(new JSON("{\"key\":1}"), Arrays.asList(new AbstractMap.SimpleEntry<>("key", 1)));
		nextPack();

		testIteratorObject(new JSON("null"), new HashSet<>());
		testIteratorObject(new JSON("true"), ClassCastException.class);
		testIteratorObject(new JSON("false"), ClassCastException.class);
		testIteratorObject(new JSON("0"), ClassCastException.class);
		testIteratorObject(new JSON("0.5"), ClassCastException.class);
		testIteratorObject(new JSON("\"ASD\""), ClassCastException.class);
		testIteratorObject(new JSON("[\"ASD\",\"SAD\",\"DSA\"]"), ClassCastException.class);
		testIteratorObject(new JSON("{\"key\":1, \"key2\":true}"), new HashSet<>(Arrays.asList(new AbstractMap.SimpleEntry<>("key", 1), new AbstractMap.SimpleEntry<>("key2", true))));
		nextPack();

		//---------- TEST ArrayIndex ---------------
		testArrayIndex(new JSON("[0,5,10]"), 0, 0);
		testArrayIndex(new JSON("[0,5,10]"), 5, 1);
		testArrayIndex(new JSON("[0,5,10]"), 10, 2);
		testArrayIndex(new JSON("[0,5,10]"), 100, -1);
		nextPack();

		//---------- TEST ArraySet ---------------
		testArraySet(new JSON("[0,\"5\",false]"), 0, 100, null);
		testArraySet(new JSON("[0,\"5\",false]"), 1, "100", null);
		testArraySet(new JSON("[0,\"5\",false]"), 2, true, null);
		testArraySet(new JSON("[0,\"5\",false]"), 3, 0, IndexOutOfBoundsException.class);
		testArraySet(new JSON(JSON.JSONVALUE_TYPE.JSON_OBJECT), 3, 0, ClassCastException.class);
		testArraySet(new JSON(JSON.JSONVALUE_TYPE.TRUE), 3, 0, ClassCastException.class);
		nextPack();

		//---------- TEST Validator ---------------
		testValidator(new JSON(), new JSON("false"), false);
		testValidator(new JSON(), new JSON("true"), true);
		testValidator(new JSON(), new JSON("{}"), true);

		testValidator(new JSON("null"), new JSON("false"), false);
		testValidator(new JSON("null"), new JSON("true"), true);
		testValidator(new JSON("null"), new JSON("{}"), true);

		testValidator(new JSON("true"), new JSON("false"), false);
		testValidator(new JSON("true"), new JSON("true"), true);
		testValidator(new JSON("true"), new JSON("{}"), true);
		testValidator(new JSON("true"), new JSON("{\"type\": \"boolean\"}"), true);
		testValidator(new JSON("true"), new JSON("{\"type\": \"string\"}"), false);

		testValidator(new JSON("false"), new JSON("false"), false);
		testValidator(new JSON("false"), new JSON("true"), true);
		testValidator(new JSON("false"), new JSON("{}"), true);
		testValidator(new JSON("false"), new JSON("{\"type\": \"boolean\"}"), true);
		testValidator(new JSON("false"), new JSON("{\"type\": \"string\"}"), false);

		testValidator(new JSON("\"asd\""), new JSON("false"), false);
		testValidator(new JSON("\"asd\""), new JSON("true"), true);
		testValidator(new JSON("\"asd\""), new JSON("{}"), true);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"string\"}"), true);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"string\", \"enum\": [\"asd\"]}"), true);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"string\", \"enum\": [\"dsa\", \"asd\"]}"), true);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"string\", \"minLength\": 2, \"maxLength\": 4}"), true);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"string\", \"minLength\": 4, \"maxLength\": 4}"), false);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"string\", \"minLength\": 2, \"maxLength\": 2}"), false);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"string\", \"enum\": [\"dsa\"]}"), false);
		testValidator(new JSON("\"asd\""), new JSON("{\"type\": \"number\"}"), false);

		testValidator(new JSON("0"), new JSON("false"), false);
		testValidator(new JSON("0"), new JSON("true"), true);
		testValidator(new JSON("0"), new JSON("{}"), true);
		testValidator(new JSON("0"), new JSON("{\"type\": \"number\"}"), true);
		testValidator(new JSON("0"), new JSON("{\"type\": \"integer\"}"), true);
		testValidator(new JSON("0"), new JSON("{\"type\": \"integer\", \"minimum\": -10, \"maximum\": 10}"), true);
		testValidator(new JSON("0"), new JSON("{\"type\": \"integer\", \"minimum\": 1, \"maximum\": 10}"), false);
		testValidator(new JSON("0"), new JSON("{\"type\": \"integer\", \"minimum\": -10, \"maximum\": -1}"), false);
		testValidator(new JSON("0"), new JSON("{\"type\": \"string\"}"), false);

		testValidator(new JSON("5.5"), new JSON("false"), false);
		testValidator(new JSON("5.5"), new JSON("true"), true);
		testValidator(new JSON("5.5"), new JSON("{}"), true);
		testValidator(new JSON("5.5"), new JSON("{\"type\": \"number\"}"), true);
		testValidator(new JSON("5.5"), new JSON("{\"type\": \"integer\"}"), false);
		testValidator(new JSON("5.5"), new JSON("{\"type\": \"number\", \"minimum\": -10, \"maximum\": 10}"), true);
		testValidator(new JSON("5.5"), new JSON("{\"type\": \"number\", \"minimum\": 6, \"maximum\": 10}"), false);
		testValidator(new JSON("5.5"), new JSON("{\"type\": \"number\", \"minimum\": -10, \"maximum\": 4}"), false);
		testValidator(new JSON("5.5"), new JSON("{\"type\": \"string\"}"), false);

		testValidator(new JSON("[]"), new JSON("false"), false);
		testValidator(new JSON("[]"), new JSON("true"), true);
		testValidator(new JSON("[]"), new JSON("{}"), true);
		testValidator(new JSON("[]"), new JSON("{\"type\": \"array\"}"), true);
		testValidator(new JSON("[]"), new JSON("{\"type\": \"array\", \"minItems\": -1}"), true);
		testValidator(new JSON("[]"), new JSON("{\"type\": \"array\", \"minItems\": 1}"), false);
		testValidator(new JSON("[]"), new JSON("{\"type\": \"array\", \"maxItems\": 1}"), true);
		testValidator(new JSON("[]"), new JSON("{\"type\": \"array\", \"maxItems\": -1}"), false);
		testValidator(new JSON("[1,2,3]"), new JSON("{\"type\": \"array\", \"minItems\": 3, \"maxItems\": 3}"), true);
		testValidator(new JSON("[1,2,3,4]"), new JSON("{\"type\": \"array\", \"minItems\": 3, \"maxItems\": 3}"), false);
		testValidator(new JSON("[1,2,3]"), new JSON("{\"type\": \"array\", \"minItems\": 3, \"maxItems\": 3, \"items\": { \"type\": \"number\"}}"), true);

		testValidator(new JSON("{}"), new JSON("false"), false);
		testValidator(new JSON("{}"), new JSON("true"), true);
		testValidator(new JSON("{}"), new JSON("{}"), true);
		testValidator(new JSON("{}"), new JSON("{\"type\": \"object\"}"), true);
		testValidator(new JSON("{\"asd\": 1, \"dsa\": 2}"), new JSON("{\"type\": \"object\", \"required\": [\"asd\", \"dsa\"]}"), true);
		testValidator(new JSON("{\"dsa\": 2}"), new JSON("{\"type\": \"object\", \"required\": [\"asd\", \"dsa\"]}"), false);
		testValidator(new JSON("{\"asd\": 1}"), new JSON("{\"type\": \"object\", \"required\": [\"asd\", \"dsa\"]}"), false);
		testValidator(new JSON("{\"asd\": 1, \"dsa\": 2}"), new JSON("{\"type\": \"object\", \"required\": []}"), true);
		testValidator(new JSON("{\"asd\": 1, \"dsa\": 2}"), new JSON("{\"type\": \"object\", \"properties\": {\"asd\": {\"type\": \"number\"}}, \"required\": [\"asd\", \"dsa\"]}"), true);
		testValidator(new JSON("{\"dsa\": 2}"), new JSON("{\"type\": \"object\", \"properties\": {\"asd\": {\"type\": \"number\"}}, \"required\": [\"dsa\"]}"), true);
		testValidator(new JSON("{\"asd\": 1, \"dsa\": 2}"), new JSON("{\"type\": \"object\", \"properties\": {\"asd\": {\"type\": \"string\"}}, \"required\": [\"asd\", \"dsa\"]}"), false);
		testValidator(new JSON("[]"), new JSON("{\"type\": [\"array\", \"string\"]}"), true);
		testValidator(new JSON("\"ASDASD\""), new JSON("{\"type\": [\"array\", \"string\"]}"), true);

		testValidator(new JSON(), new JSON(), IllegalArgumentException.class);
		testValidator(new JSON(), new JSON("{\"type\": 1}"), IllegalArgumentException.class);

		//======================================================================
		System.out.println("");
		if (failed) {
			System.out.println(failedcount + " out of " + (passedcount + failedcount) + " failed");
		} else {
			System.out.println("All " + passedcount + " tests passed.");
		}
		System.exit(failed ? 1 : 0);
	}

	public static void test(JSON.JSONVALUE_TYPE _type, Object _value) {
		JSON j = new JSON(_type);
		if (j.type_get() == _type && equality(_type, j.json_get(), _value)) {
			passed();
		} else {
			failed(_type.toString());
		}
	}

	public static void test(String _input, JSON.JSONVALUE_TYPE _type, Object _value) {
		JSON j = new JSON(_input);
		if (j.type_get() == _type && equality(_type, j.json_get(), _value)) {
			passed();
		} else {
			failed(_input);
		}
	}

	public static void test(String _input, Class<? extends Exception> _c) {
		try {
			JSON j = new JSON(_input);
			failed(_input);
		} catch (Exception ex) {
			if (ex.getClass() == _c) {
				passed();
			} else {
				failed(_input);
			}
		}
	}

	public static void test(String _input, String _expected) {
		JSON j = new JSON(_input);
		if (!j.json_toString(false).equals(_expected)) {
			failed(_input);
			return;
		}
		String whitespace = j.json_toString(true);
		JSON j2 = new JSON(whitespace);
		if (!j2.json_toString(false).equals(_expected)) {
			failed(_input);
			return;
		}
		passed();
	}

	public static void test(String _input) {
		JSON j = new JSON(_input);
		JSON j2 = new JSON(j);
		JSON j3 = new JSON().json_init(j);
		JSON j4 = j.copy();

		if (!j.json_toString(false).equals(_input)) {
			failed(_input);
			return;
		}

		j.json_init();

		if (!j2.json_toString(false).equals(_input)) {
			failed(_input);
			return;
		}

		if (!j3.json_toString(false).equals(_input)) {
			failed(_input);
			return;
		}

		if (!j4.json_toString(false).equals(_input)) {
			failed(_input);
			return;
		}

		if (j == j2) {
			failed(_input);
			return;
		}

		if (j == j3) {
			failed(_input);
			return;
		}

		if (j == j4) {
			failed(_input);
			return;
		}

		passed();
	}

	private static void testObject(JSON.JSONVALUE_TYPE _type, Object _entry) {
		JSON j = new JSON();

		if (j.json_containsKey("key") || j.json_size() != 0) {
			failed(j.toString());
			return;
		}

		if (j.json_containsValue(_entry)) {
			failed(j.toString());
			return;
		}

		j.json_add("key", _entry);

		if (!j.json_containsKey("key") || j.json_size() != 1) {
			failed(j.toString());
			return;
		}

		if (j.json_get("key2") != null) {
			failed(j.toString());
			return;
		}

		if (!j.json_containsValue(_entry)) {
			failed(j.toString());
			return;
		}

		Object ret = j.json_get("key");

		switch (_type) {
			case NULL:
				if (ret != null) {
					failed(j.toString());
					return;
				}
				break;
			case TRUE:
				if ((boolean) ret != true) {
					failed(j.toString());
					return;
				}
				if (j.json_getBoolean("key") != true) {
					failed(j.toString());
					return;
				}
				if (j.<Boolean>json_getCast("key") != true) {
					failed(j.toString());
					return;
				}
				break;
			case FALSE:
				if ((boolean) ret != false) {
					failed(j.toString());
					return;
				}
				if (j.json_getBoolean("key") != false) {
					failed(j.toString());
					return;
				}
				if (j.<Boolean>json_getCast("key") != false) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_NUMBER:
				if (!(_entry instanceof Float) && ret != _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Integer && j.json_getInt("key") != (int) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Long && j.json_getLong("key") != (long) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Double && j.json_getDouble("key") != (double) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Float && j.json_getFloat("key") != (float) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Integer && j.<Integer>json_getCast("key") != (int) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Long && j.<Long>json_getCast("key") != (long) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Double && j.<Double>json_getCast("key") != (double) _entry) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_STRING:
				if (!ret.equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.json_getString("key").equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.<String>json_getCast("key").equals(_entry)) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_ARRAY:
				if (!ret.equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.json_getArray("key").equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.<JSON>json_getCast("key").equals(_entry)) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_OBJECT:
				if (!ret.equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.json_getObject("key").equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.<JSON>json_getCast("key").equals(_entry)) {
					failed(j.toString());
					return;
				}
				break;
		}

		passed();
	}

	private static void testArray(JSON.JSONVALUE_TYPE _type, Object _entry) {
		JSON j = new JSON();

		if (j.json_containsKey(0) || j.json_size() != 0) {
			failed(j.toString());
			return;
		}

		if (j.json_containsValue(_entry)) {
			failed(j.toString());
			return;
		}

		j.json_add(_entry);

		if (!j.json_containsValue(_entry)) {
			failed(j.toString());
			return;
		}

		if (j.json_get(1) != null) {
			failed(j.toString());
			return;
		}

		if (!j.json_containsKey(0) || j.json_size() != 1) {
			failed(j.toString());
			return;
		}

		Object ret = j.json_get(0);

		switch (_type) {
			case NULL:
				if (ret != null) {
					failed(j.toString());
					return;
				}
				break;
			case TRUE:
				if ((boolean) ret != true) {
					failed(j.toString());
					return;
				}
				if (j.json_getBoolean(0) != true) {
					failed(j.toString());
					return;
				}
				if (j.<Boolean>json_getCast(0) != true) {
					failed(j.toString());
					return;
				}
				break;
			case FALSE:
				if ((boolean) ret != false) {
					failed(j.toString());
					return;
				}
				if (j.json_getBoolean(0) != false) {
					failed(j.toString());
					return;
				}
				if (j.<Boolean>json_getCast(0) != false) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_NUMBER:
				if (!(_entry instanceof Float) && ret != _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Integer && j.json_getInt(0) != (int) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Long && j.json_getLong(0) != (long) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Double && j.json_getDouble(0) != (double) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Float && j.json_getFloat(0) != (float) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Integer && j.<Integer>json_getCast(0) != (int) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Long && j.<Long>json_getCast(0) != (long) _entry) {
					failed(j.toString());
					return;
				}
				if (_entry instanceof Double && j.<Double>json_getCast(0) != (double) _entry) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_STRING:
				if (!ret.equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.json_getString(0).equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.<String>json_getCast(0).equals(_entry)) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_ARRAY:
				if (!ret.equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.json_getArray(0).equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.<JSON>json_getCast(0).equals(_entry)) {
					failed(j.toString());
					return;
				}
				break;
			case JSON_OBJECT:
				if (!ret.equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.json_getObject(0).equals(_entry)) {
					failed(j.toString());
					return;
				}
				if (!j.<JSON>json_getCast(0).equals(_entry)) {
					failed(j.toString());
					return;
				}
				break;
		}

		passed();
	}

	private static void testIterator(JSON _json, List<Object> _list) {
		int index = 0;
		for (Object o : _json) {
			if (o == null && _list.get(index) == null) {
				index++;
				continue;
			}
			if (!o.equals(_list.get(index))) {
				failed(_json.toString());
				return;
			}
			index++;
		}
		if (index < _list.size()) {
			failed(_json.toString());
			return;
		}
		passed();
	}

	private static <T> void testIteratorCast(JSON _json, List<T> _list) {
		int index = 0;
		for (T o : _json.<T>iteratorCast()) {
			if (!o.equals(_list.get(index))) {
				failed(_json.toString());
				return;
			}
			index++;
		}
		if (index < _list.size()) {
			failed(_json.toString());
			return;
		}
		passed();
	}

	private static void testIteratorObject(JSON _json, HashSet<Map.Entry<String, Object>> _list) {
		int index = 0;
		for (Map.Entry<String, Object> o : _json.iteratorObject()) {
			if (!_list.contains(o)) {
				failed(_json.toString());
				return;
			}
			index++;
		}
		if (index < _list.size()) {
			failed(_json.toString());
			return;
		}
		passed();
	}

	private static void testIteratorObject(JSON _json, Class<? extends Exception> _ex) {
		try {
			for (Map.Entry<String, Object> o : _json.iteratorObject()) {
				failed(_json.toString());
				return;
			}
		} catch (Exception ex) {
			if (ex.getClass() == _ex) {
				passed();
				return;
			}
		}

		failed(_json.toString());
	}

	private static <T> void testIteratorCast(JSON _json, List<T> _list, Class<? extends Exception> _ex) {
		int index = 0;
		try {
			for (T o : _json.<T>iteratorCast()) {
				if (!o.equals(_list.get(index))) {
					failed(_json.toString());
					return;
				}
				index++;
			}
		} catch (Exception ex) {
			if (ex.getClass() == _ex) {
				passed();
				return;
			}
		}

		failed(_json.toString());
	}

	private static void testArrayIndex(JSON _json, Object _element, int _index) {
		int indexof = _json.json_indexOf(_element);

		if (_index != indexof) {
			failed(_json.toString());
			return;
		}

		_json.json_removeValue(_element);

		passed();
	}

	private static void testArraySet(JSON _json, int _index, Object _after, Class<? extends Exception> _class) {
		try {
			_json.json_set(_index, _after);
		} catch (Exception ex) {
			if (_class == null || _class != ex.getClass()) {
				failed(ex.getMessage());
				return;
			} else {
				passed();
				return;
			}
		}

		if (!_json.json_get(_index).equals(_after)) {
			failed(_json.json_get(_index) + " does not equal " + _after);
			return;
		}

		passed();
	}

	private static void testValidator(JSON _json, JSON _validator, boolean _validate) {
		if (_json.json_validate(_validator) != _validate) {
			failed(_json.toString());
			return;
		}

		passed();
	}

	private static void testValidator(JSON _json, JSON _validator, Class<? extends Exception> _exception) {
		try {
			_json.json_validate(_validator);
			failed(_json.toString());
			return;
		} catch (Exception ex) {
			if (ex.getClass() != _exception) {
				failed(_json.toString());
				return;
			}
		}
		passed();
	}

	private static boolean equality(JSON.JSONVALUE_TYPE _type, Object _got, Object _expected) {
		try {
			switch (_type) {
				case NULL:
					return (_got == null);
				case FALSE:
					return ((boolean) _got == false);
				case TRUE:
					return ((boolean) _got == true);
				case JSON_STRING:
					return (_got.equals(_expected));
				case JSON_NUMBER:
					return (((Number) _got).equals((Number) _expected));
				case JSON_ARRAY: {
					JSON got = (JSON) _got;
					ArrayList<Object> expected = ((ArrayList<Object>) _expected);
					if (got.json_size() != expected.size()) {
						return (false);
					}
					for (int i = 0; i < got.json_size(); i++) {
						if (!equality(got.json_getType(i), got.json_get(i), expected.get(i))) {
							return (false);
						}
					}
					return (true);
				}
				case JSON_OBJECT: {
					JSON got = ((JSON) _got);
					HashMap<String, Object> expected = ((HashMap<String, Object>) _expected);
					if (got.json_size() != expected.size()) {
						return (false);
					}
					for (Map.Entry<String, Object> entry : expected.entrySet()) {
						if (!equality(got.json_getType(entry.getKey()), got.json_get(entry.getKey()), entry.getValue())) {
							return (false);
						}
					}
					return (true);
				}
			}
		} catch (NullPointerException | ClassCastException ex) {

		}
		return (false);
	}

	private static void nextPack() {
		pack++;
		no = 0;
		System.out.println("======= Test bundle #" + pack + " =======");
	}

	private static void passed() {
		System.out.println("Test #" + no + " in " + pack + " passed.");
		passedcount++;
		no++;
	}

	private static void failed(String _input) {
		System.out.println("Test #" + no + " in " + pack + " failed!");
		System.err.println(_input);
		failed = true;
		failedcount++;
		no++;
	}

}
