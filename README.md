# JSON Object
-----------

* Public domain code. Check [LICENSE](LICENSE)

* Single file, leight weight, easy to use JSON parser.

* Correctly implements https://www.json.org/json-en.html

* Basic (__not fully implemented__) validation using https://json-schema.org/

* Test file for validating functionality.


### What is special in this implementation?

* More white space characters are allowed:
	- JSON specification allows "", u0020, u000A, u000D, u0009
	- This implementation allows every Character defined by Character.isWhitespace(char)
* Types are converted to Java number types:
	- JSON specification allows arbitrary length integers and arbitrary precision floating point values.
	- This implementation can read Java signed Integers and automatically convert them to a Java signed Long, when Integer.MAX_VALUE is exceeded.No automatic conversion to BigInteger. All read floating point values will be converted to a Java double.
	- Upon insertion of a Java float, this implementation will automatically convert it to a double.


### API Overview (not all methods listed here)

Constructors:
```
new JSON();
//Creates a new empty JSON.

new JSON(java.lang.String _input);
//Creates a new JSON and parses the specified String

new JSON(byte[] _input, java.nio.charset.Charset _encoding);
//Creates a new JSON and parses the specified input

new JSON(java.nio.ByteBuffer _input, java.nio.charset.Charset _encoding);
//Creates a new JSON and parses the specified input

new JSON(java.nio.CharBuffer _input);
//Creates a new JSON and parses the specified input

new JSON(java.io.File _file);
//Creates a new JSON and parses the specified file

new JSON(java.io.InputStream _input);
//Creates a new JSON and parses the specified input stream

new JSON(JSON _other);
//Creates a new JSON and deep copies all values of the specified JSON.
```

Add values using ``json_add``:
```
json.json_add(java.lang.Object _value);
//Adds the given value to this JSON. (Array)

json.json_add(java.lang.String _key, java.lang.Object _value);
//Adds the given value to this JSON with the given key. (Object)
```

Retrieve values using various ``json_get`` methods:
```
json_get()
json_get(int _key)
json_get(java.lang.String _key)
json_getArray(int _key)
json_getArray(java.lang.String _key)
json_getBoolean(int _key)
json_getBoolean(java.lang.String _key)
<T>json_getCast(int _key)
<T>json_getCast(java.lang.String _key)
json_getDouble(int _key)
json_getDouble(java.lang.String _key)
json_getFloat(int _key)
json_getFloat(java.lang.String _key)
json_getInt(int _key)
json_getInt(java.lang.String _key)
json_getLong(int _key)
json_getLong(java.lang.String _key)
json_getObject(int _key)
json_getObject(java.lang.String _key)
json_getString(int _key)
json_getString(java.lang.String _key)
json_getType(int _key)
json_getType(java.lang.String _key)
```


JSON implements ``Iterable<Object>`` so you can do:
```
for(Object o : json) {
  //[...]
}
```

Conveniently use:
```
json_writeToFile(java.io.File _destination, int _bufferSize, boolean _whitespaces);
// Converts this JSON into a parsable String representation with or without added white space and streams the result to a given file.

json_writeToStream(java.io.OutputStream _destination, int _bufferSize, boolean _whitespaces);
//Converts this JSON into a parsable String representation with or without added white space and streams the result to a given stream.
```





